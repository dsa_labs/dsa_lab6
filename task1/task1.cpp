﻿#include <iostream>
#include <random> 
#include <math.h>
#include <vector>
#include <chrono>
#include <fstream>

#define GETTIME std::chrono::high_resolution_clock::now()
#define DURATION(a) std::chrono::duration_cast<std::chrono::microseconds>(a).count()

void printArr(short arr[], int n)
{
	for (int i = 0; i < n; i++)
		std::cout << arr[i] << " ";
	std::cout << "\n";
}

void heapify(float arr[], int n, int i)
{
	// Визначаємо індекс найбільшого елементу серед поточного вузла, лівого та правого дочірніх вузлів
	int largest = i;
	float l = 2 * i + 1;
	float r = 2 * i + 2;
	if (l < n && arr[(int)l] > arr[largest])
		largest = l;
	if (r < n && arr[(int)r] > arr[largest])
		largest = r;

	// Якщо найбільший елемент не знаходиться в поточному вузлі, міняємо їх місцями та рекурсивно викликаємо heapify для нового вузла
	if (largest != i)
	{
		std::swap(arr[i], arr[largest]);
		heapify(arr, n, largest);
	}
}

void heapSort(float arr[], int n)
{
	std::ofstream file("output.txt", std::ios::app);
	auto begin = GETTIME;
	// Формуємо кучу, починаючи з останнього вузла, що має дочірні вузли
	for (int i = n / 2 - 1; i >= 0; i--)
		heapify(arr, n, i);
	// Сортуємо кучу: переміщуємо найбільший елемент на кінець та зменшуємо розмір кучі
	for (int i = n - 1; i > 0; i--)
	{
		std::swap(arr[0], arr[i]);
		heapify(arr, i, 0);
	}
	auto end = GETTIME;
	file << n << "\n\n";

	file << DURATION(end - begin) << "\n";
	std::cout << DURATION(end - begin) << "\n";
	file.close();
}

void shellSort(int arr[], int n) {
	std::ofstream file("output.txt", std::ios::app);
	auto begin = GETTIME;
	// Цикл для вибору кроку сортування
	for (int gap = 1; gap > 0; gap += pow(2, gap) - 1) {
		// Цикл для сортування підмасивів з поточним кроком
		for (int i = gap; i < n; i += 1) {
			// Зберігаємо поточний елемент у тимчасовій змінній
			int temp = arr[i];
			int j;
			// Цикл для вставки поточного елемента у відсортований підмасив
			for (j = i; j >= gap && arr[j - gap] > temp; j -= gap) {
				// Зсуваємо елементи підмасиву вправо, щоб звільнити місце для вставки
				arr[j] = arr[j - gap];
			}
			// Вставляємо поточний елемент у відсортований підмасив
			arr[j] = temp;
		}
	}
	auto end = GETTIME;
	file << n << "\n\n";

	file << DURATION(end - begin) << "\n";
	std::cout << DURATION(end - begin) << "\n";
	file.close();
}

void countSort(std::vector<short>& arr) {
	std::ofstream file("output.txt", std::ios::app);
	auto begin = GETTIME;

	// Знаходимо максимальне та мінімальне значення масиву
	int maxVal = *std::max_element(arr.begin(), arr.end());
	int minVal = *std::min_element(arr.begin(), arr.end());

	// Створюємо масив для підрахунку кількості кожного елемента
	std::vector<int> count(maxVal - minVal + 1);

	// Підраховуємо кількість кожного елемента
	for (int i = 0; i < arr.size(); i++) {
		count[arr[i] - minVal]++;
	}
	// Знаходимо кумулятивну суму, щоб знайти позицію кожного елемента у відсортованому масиві
	for (int i = 1; i < count.size(); i++) {
		count[i] += count[i - 1];
	}

	std::vector<short> res(arr.size());
	// Заповнюємо відсортований масив
	for (int i = arr.size() - 1; i >= 0; i--) {
		res[count[arr[i] - minVal] - 1] = arr[i];
		count[arr[i] - minVal]--;
	}
	arr = res;
	auto end = GETTIME;
	file << arr.size() << "\n";

	file << DURATION(end - begin) << "\n\n";
	std::cout << DURATION(end - begin) << "\n";
	file.close();

}

int main()
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_real_distribution<> heapDis(-100, 10);
	std::uniform_real_distribution<> shellDis(0, 400);
	std::uniform_real_distribution<> countDis(-10, 100);
 	const int n = 10000;
	int arr[n];
	float heapArr[n];
	std::vector<short> countArr;

	for (size_t i = 0; i < n; i++)
	{
		countArr.push_back(countDis(gen));
	}

	std::cout << std::endl;
	countSort(countArr);
}
